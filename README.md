# Selfhost-Updater

This runs a dynDNS updater which ensures that the domain name points to the correct IP address.

If you have more generic questions please have a look at the [Docker Image](https://hub.docker.com/repository/docker/mmprivat/selfhost-updater).

## Getting Started

For starting the service initially just copy the `.env-example` to `.env` and edit the variables so they fit your environment.

You can also add some more variables like disabling SSL or setting your own URL for updating the dyndns.

After that, you can start the container.

### Upgrading the service / containers

For upgrading the container, you will need to call

	# docker-compose down; docker-compose pull; docker-compose up -d

If you put in your dyndns settings in another way then via `.env` please ensure that it still works with the new container.

## About the project

### Versioning

We use MAIN.MINOR number for versioning where as MAIN usually means big / breaking changes and MINOR usually means small / non breaking changes. For the version number, see at the top header or in the [tags on this repository](https://bitbucket.org/mmprivat/selfhost-updater/downloads/?tab=tags).

### Author(s)

* **Manuel Manhart** - *Initial work*

### License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

### Contributing

If you want to contribute, contact the author or create a pull request.

### Changelog

__1.0__

* Created a new bitbucket repo for the container project
* Switched configuration for the dyndns updater from a mounted volume to `.env`

### Open issues

* No support for multiple domains / configurations - one container for each is pretty much an overkill

## Sources

* [Docker](http://www.docker.io/) - The container base
* [Docker-Compose](https://docs.docker.com/compose/) - Composing multiple containers into one service
* [VS Code](https://code.visualstudio.com/) - Used to edit all the files
* [Docker Image](https://hub.docker.com/repository/docker/mmprivat/selfhost-updater)
